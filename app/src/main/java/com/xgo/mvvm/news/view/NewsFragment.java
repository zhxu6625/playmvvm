package com.xgo.mvvm.news.view;

import android.view.View;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.xgo.mvvm.BR;
import com.xgo.mvvm.R;
import com.xgo.mvvm.base.lifecycle.BaseLifecycleObserver;
import com.xgo.mvvm.base.view.BaseFragment;
import com.xgo.mvvm.databinding.FragmentNewsBinding;
import com.xgo.mvvm.news.NewsViewModel;
import com.xgo.mvvm.news.lifecycle.NewsLifecycle;
import com.xgo.mvvm.news.model.NewsResponse;
import com.xgo.mvvm.news.view.adapter.NewsAdapter;

/**
 * @author substring
 * @date 2019/5/27
 * email:xuan.zhang@xgo.one
 */
public class NewsFragment extends BaseFragment<FragmentNewsBinding> {

    private NewsAdapter mAdapter;

    private NewsViewModel mNewsViewModel;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_news;
    }

    @Override
    protected void initViewModel() {
        mNewsViewModel = ViewModelProviders.of(this).get(NewsViewModel.class);
        mNewsViewModel.provider().observe(this, new Observer<NewsResponse>() {
            @Override
            public void onChanged(NewsResponse newsResponse) {
                mViewDataBinding.setVariable(BR.model, newsResponse);
            }
        });
    }

    @Override
    protected void initView() {
        mViewDataBinding.b1.setText("按钮1");
        mViewDataBinding.b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Bundle bundle = new Bundle();
//                bundle.putString("title", "aaaaaaaaaaaaaaaaaaaaaaaaaa");
//                Navigation.findNavController(view).navigate(R.id.action_newsFragment_to_blankFragment, bundle);

//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("www.baidu.com"));
//                startActivity(intent);
//                Navigation.findNavController(view).handleDeepLink(intent);

//                mAdapter.remove(0);

                NewsDialog dialog = new NewsDialog();
                dialog.show(getChildFragmentManager(), "dialog");
            }
        });
        mViewDataBinding.b2.setText("按钮2");
        mViewDataBinding.b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Bundle bundle = new Bundle();
//                bundle.putString("title", "bbbbbbbbbbbbbbbbbbbbbbbbb");
//                Navigation.findNavController(view).navigate(R.id.action_newsFragment_to_blankFragment2, bundle);
                mNewsViewModel.tryRequestNewsData();
            }
        });

        mAdapter = new NewsAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

        mViewDataBinding.rv1.setLayoutManager(layoutManager);
        mViewDataBinding.rv1.setAdapter(mAdapter);

        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setAddDuration(300);
        defaultItemAnimator.setRemoveDuration(300);

        mViewDataBinding.rv1.setItemAnimator(defaultItemAnimator);
    }

    @Override
    protected BaseLifecycleObserver initLifecycle() {
        return new NewsLifecycle();
    }
}
