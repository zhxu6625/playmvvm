package com.xgo.mvvm.news.view;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.xgo.mvvm.R;
import com.xgo.mvvm.base.widget.dialog.BaseDialogFragment;

/**
 * @author substring
 * @date 2019/6/3
 * email:xuan.zhang@xgo.one
 */
public class NewsDialog extends BaseDialogFragment {

    @NonNull
    @Override
    protected View initView() {
        setCancelable(false);
        View view = View.inflate(getContext(), R.layout.dialog_news, null);
        TextView txt = view.findViewById(R.id.txt);
        txt.setText("点我关闭");

        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;
    }
}
