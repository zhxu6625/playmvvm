package com.xgo.mvvm.news.view.bindingadapter;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.xgo.mvvm.news.model.NewsResponse;
import com.xgo.mvvm.news.view.adapter.NewsAdapter;

import java.util.List;

/**
 * @author substring
 * @date 2019/5/28
 * email:xuan.zhang@xgo.one
 */
public class NewsBindingAdapter {

    @BindingAdapter({"imageUrl"})
    public static void imageUrl(ImageView imageView, String url) {
//        Glide.with(imageView.getContext()).load(url).into(imageView);
    }

    @BindingAdapter({"refreshData"})
    public static void refreshData(RecyclerView recyclerView, List<NewsResponse> listData) {
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter != null && listData != null) {
            ((NewsAdapter) adapter).refresh(listData);
        }
    }
}
