package com.xgo.mvvm.news.model;

import com.xgo.mvvm.base.model.BaseResponse;

import java.util.List;

/**
 * @author substring
 * @date 2019/5/27
 * email:xuan.zhang@xgo.one
 */
public class NewsResponse extends BaseResponse {

    public String title;
    public String content;
    public String url;
    public List<NewsResponse> items;
}
