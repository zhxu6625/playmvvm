package com.xgo.mvvm.news.livedata;

import com.xgo.mvvm.base.livedata.BaseLiveData;
import com.xgo.mvvm.news.model.NewsResponse;

import java.util.ArrayList;

/**
 * @author substring
 * @date 2019/5/27
 * email:xuan.zhang@xgo.one
 */
public class NewsLiveData extends BaseLiveData<NewsResponse> {

    public NewsLiveData() {
        super();
    }

    /**
     * Mock TEST
     */
    public void tryRequestNewsData() {
        NewsResponse response = new NewsResponse();
        response.title = "asdadaadadada";
        response.content = "#00ff66";
        response.items = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            NewsResponse item = new NewsResponse();
            item.url = "asdsadsa" + i;
            item.content = "asdsadsa" + i;
            item.title = "1231" + i;
            response.items.add(item);
        }
        update(response);
    }
}
