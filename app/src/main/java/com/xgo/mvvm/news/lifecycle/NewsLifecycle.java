package com.xgo.mvvm.news.lifecycle;

import android.util.Log;

import com.xgo.mvvm.base.lifecycle.BaseLifecycleObserver;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;

/**
 * @author substring
 * @date 2019/5/27
 * email:xuan.zhang@xgo.one
 */
public class NewsLifecycle extends BaseLifecycleObserver {

    @Override
    protected void onAny(LifecycleOwner owner, Lifecycle.Event event) {
        Log.d("SUBSTRING", "NewsLifecycle onAny event.toString()：" + event.toString() + " event.name()：" + event.name() + " getCurrentState()：" + owner.getLifecycle().getCurrentState());
    }
}
