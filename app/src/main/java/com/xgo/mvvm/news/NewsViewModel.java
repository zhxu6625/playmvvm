package com.xgo.mvvm.news;

import android.app.Application;

import com.xgo.mvvm.base.BaseViewModel;
import com.xgo.mvvm.databinding.FragmentNewsBinding;
import com.xgo.mvvm.news.livedata.NewsLiveData;
import com.xgo.mvvm.news.model.NewsResponse;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.LiveData;

/**
 * @author substring
 * @date 2019/5/22
 * email:xuan.zhang@xgo.one
 */
public class NewsViewModel extends BaseViewModel<NewsLiveData, FragmentNewsBinding> {

    public NewsViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void injectionVDataBinding(FragmentNewsBinding fragmentNewsBinding) {
        mViewDataBinding = fragmentNewsBinding;
    }

    @Override
    protected NewsLiveData initLiveData() {
        return new NewsLiveData();
    }

    public void tryRequestNewsData() {
        mLiveData.tryRequestNewsData();
    }
}
