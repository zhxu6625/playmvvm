package com.xgo.mvvm.news.view.adapter;

import com.xgo.mvvm.BR;
import com.xgo.mvvm.R;
import com.xgo.mvvm.base.widget.recyclerview.BaseAdapter;
import com.xgo.mvvm.databinding.FragmentNewsBinding;
import com.xgo.mvvm.news.model.NewsResponse;

/**
 * @author substring
 * @date 2019/6/3
 * email:xuan.zhang@xgo.one
 */
public class NewsAdapter extends BaseAdapter<NewsResponse, FragmentNewsBinding> {

    public NewsAdapter() {
        super(R.layout.item_news, BR.newsModel);
    }
}
