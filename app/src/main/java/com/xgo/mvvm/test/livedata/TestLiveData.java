package com.xgo.mvvm.test.livedata;

import android.util.Log;

import com.xgo.mvvm.base.livedata.BaseLiveData;
import com.xgo.mvvm.base.model.BaseSource;
import com.xgo.mvvm.network.api.API;
import com.xgo.mvvm.test.model.TestResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author substring
 * @date 2019/5/27
 * email:xuan.zhang@xgo.one
 */
public class TestLiveData extends BaseLiveData<TestResponse> {

    public TestLiveData() {
        super();
    }

    /**
     * Mock TEST
     */
    public void tryRequestTestData() {
        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl("http://api.moto-traveller.xgo.work/").build();
        Call<BaseSource<TestResponse>> call = retrofit.create(API.class).getStaticConfig();
        call.enqueue(new Callback<BaseSource<TestResponse>>() {
            @Override
            public void onResponse(Call<BaseSource<TestResponse>> call, Response<BaseSource<TestResponse>> response) {
                BaseSource<TestResponse> source = response.body();
                if (source != null) {
                    Log.d("SUBSTRING", "Response<BaseSource<TestResponse>>：" + source.msg + " " + source.code + " " + source.data.base_price);
                    update(source.data);
                }
            }

            @Override
            public void onFailure(Call<BaseSource<TestResponse>> call, Throwable t) {

            }
        });
    }
}
