package com.xgo.mvvm.test;

import android.app.Application;

import com.xgo.mvvm.base.BaseViewModel;
import com.xgo.mvvm.test.livedata.TestLiveData;
import com.xgo.mvvm.test.model.TestResponse;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

/**
 * @author substring
 * @date 2019/5/22
 * email:xuan.zhang@xgo.one
 */
public class TestViewModel extends BaseViewModel<TestLiveData, TestResponse> {

    public TestViewModel(@NonNull final Application application) {
        super(application);
    }

    @Override
    public LiveData<TestResponse> provider() {
        return mLiveData.provider();
    }

    @Override
    protected TestLiveData initLiveData() {
        return new TestLiveData();
    }

    @Override
    public TestResponse data() {
        return mLiveData.getValue();
    }

    public void tryRequestTestData() {
        mLiveData.tryRequestTestData();
    }
}
