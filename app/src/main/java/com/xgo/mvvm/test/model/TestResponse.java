package com.xgo.mvvm.test.model;

import com.xgo.mvvm.base.model.BaseResponse;

/**
 * @author substring
 * @date 2019/5/27
 * email:xuan.zhang@xgo.one
 */
public class TestResponse extends BaseResponse {

    public String base_price;
    public String service_telephone;
    public String code_telephone;
    public String lock_time;
}
