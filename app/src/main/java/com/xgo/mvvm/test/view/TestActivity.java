package com.xgo.mvvm.test.view;

import android.view.View;

import com.xgo.mvvm.BR;
import com.xgo.mvvm.R;
import com.xgo.mvvm.base.view.BaseActivity;
import com.xgo.mvvm.base.lifecycle.BaseLifecycleObserver;
import com.xgo.mvvm.databinding.ActivityTestBinding;
import com.xgo.mvvm.news.NewsViewModel;
import com.xgo.mvvm.news.model.NewsResponse;
import com.xgo.mvvm.test.TestViewModel;
import com.xgo.mvvm.test.lifecycle.TestLifecycle;
import com.xgo.mvvm.test.model.TestResponse;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

/**
 * Activity and Fragment 只负责绑定和初始化工作，不需要任何逻辑代码
 */
public class TestActivity extends BaseActivity<ActivityTestBinding> {

    private TestViewModel mTestViewModel;

    private NewsViewModel mNewsViewModel;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_test;
    }

    @Override
    protected void initViewModel() {
        mTestViewModel = ViewModelProviders.of(this).get(TestViewModel.class);
        mNewsViewModel = ViewModelProviders.of(this).get(NewsViewModel.class);
        mTestViewModel.provider().observe(this, new Observer<TestResponse>() {
            @Override
            public void onChanged(TestResponse testResponse) {
                mViewDataBinding.setVariable(BR.testModel, testResponse);
            }
        });
        mNewsViewModel.provider().observe(this, new Observer<NewsResponse>() {
            @Override
            public void onChanged(NewsResponse newsResponse) {
                mViewDataBinding.setVariable(BR.newsModel, newsResponse);
            }
        });
    }

    @Override
    protected void initView() {
        mViewDataBinding.b1.setText("按钮1");
        mViewDataBinding.b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNewsViewModel.tryRequestNewsData();
            }
        });
        mViewDataBinding.b2.setText("按钮2");
        mViewDataBinding.b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTestViewModel.tryRequestTestData();
            }
        });
    }

    @Override
    protected BaseLifecycleObserver initLifecycle() {
        return new TestLifecycle();
    }
}
