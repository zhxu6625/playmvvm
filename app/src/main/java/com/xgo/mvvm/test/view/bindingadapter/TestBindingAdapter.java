package com.xgo.mvvm.test.view.bindingadapter;

import android.graphics.Color;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

/**
 * @author substring
 * @date 2019/5/28
 * email:xuan.zhang@xgo.one
 */
public class TestBindingAdapter {

    @BindingAdapter("colorString")
    public static void colorString(TextView layout, String colorString) {
        if (colorString == null) {
            colorString = "#000fff";
        }
        layout.setTextColor(Color.parseColor(colorString));
    }
}
