package com.xgo.mvvm.network.api;

import com.xgo.mvvm.base.model.BaseSource;
import com.xgo.mvvm.test.model.TestResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * @author substring
 * @date 2019/5/28
 * email:xuan.zhang@xgo.one
 */
public interface API {

    @GET("Config/StaticConfig/getStaticConfig")
    Call<BaseSource<TestResponse>> getStaticConfig();
}
