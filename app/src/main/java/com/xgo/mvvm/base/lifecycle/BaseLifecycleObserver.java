package com.xgo.mvvm.base.lifecycle;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;

public abstract class BaseLifecycleObserver implements LifecycleObserver {

    //TODO 构造函数等待设计
    public BaseLifecycleObserver() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
    protected abstract void onAny(LifecycleOwner owner, Lifecycle.Event event);

//    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
//    protected void onCreate() {
//        Log.d("SUBSTRING", "LifecycleObserver onCreate");
//    }
//
//    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
//    protected void onDestroy() {
//        Log.d("SUBSTRING", "LifecycleObserver onDestroy");
//    }
//
//    @OnLifecycleEvent(Lifecycle.Event.ON_START)
//    protected void onStart() {
//        Log.d("SUBSTRING", "LifecycleObserver onStart");
//    }
//
//    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
//    protected void onStop() {
//        Log.d("SUBSTRING", "LifecycleObserver onStop");
//    }
//
//    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
//    protected void onResume() {
//        Log.d("SUBSTRING", "LifecycleObserver onResume");
//    }
//
//    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
//    protected void onPause() {
//        Log.d("SUBSTRING", "LifecycleObserver onPause");
//    }
}
