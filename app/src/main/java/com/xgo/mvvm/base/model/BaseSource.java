package com.xgo.mvvm.base.model;

import java.io.Serializable;

/**
 * @author substring
 * @date 2019/5/27
 * email:xuan.zhang@xgo.one
 */

/**
 * 建于适配层
 *
 * @param <T>
 */
public class BaseSource<T> implements Serializable {

    public int code;
    public String msg;
    public T data;
}
