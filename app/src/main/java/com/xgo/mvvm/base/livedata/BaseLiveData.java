package com.xgo.mvvm.base.livedata;

import android.os.Looper;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.xgo.mvvm.base.model.BaseResponse;

/**
 * @author substring
 * @date 2019/5/27
 * email:xuan.zhang@xgo.one
 */
public abstract class BaseLiveData<T extends BaseResponse> extends LiveData<T> {

    private MutableLiveData<T> mLiveData;

    public BaseLiveData() {
        mLiveData = new MutableLiveData<>();
    }

    /**
     * @return
     */
    public LiveData<T> provider() {
        if (mLiveData == null) {
            mLiveData = new MutableLiveData<>();
        }
        return mLiveData;
    }

    /**
     * @param value
     */
    protected void update(T value) {
        if (mLiveData == null) {
            mLiveData = new MutableLiveData<>();
        }
        if (isMainThread()) {
            mLiveData.setValue(value);
        } else {
            mLiveData.postValue(value);
        }
    }

    /**
     * 判断当前是否主线程
     */
    private boolean isMainThread() {
        return Looper.getMainLooper() == Looper.myLooper();
    }
}
