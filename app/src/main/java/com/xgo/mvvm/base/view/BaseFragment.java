package com.xgo.mvvm.base.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import com.xgo.mvvm.base.lifecycle.BaseLifecycleObserver;

/**
 * @author substring
 * @date 2019/5/24
 * email:xuan.zhang@xgo.one
 */
public abstract class BaseFragment<VDataBind extends ViewDataBinding> extends Fragment {

    protected VDataBind mViewDataBinding;

    private BaseLifecycleObserver mLifecycleObserver;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mViewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        mLifecycleObserver = initLifecycle();
        getLifecycle().addObserver(mLifecycleObserver);
        initViewModel();
        initView();
        return mViewDataBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        if (mViewDataBinding != null) {
            mViewDataBinding.unbind();
        }
        if (mLifecycleObserver != null) {
            getLifecycle().removeObserver(initLifecycle());
        }
        super.onDestroyView();
    }

    /**
     * 初始化LayoutId
     */
    protected abstract int getLayoutId();

    /**
     * 初始化ViewModel
     */
    protected abstract void initViewModel();

    /**
     * 初始化视图
     */
    protected abstract void initView();

    /**
     * 初始化生命周期监听
     */
    protected abstract BaseLifecycleObserver initLifecycle();
}
