package com.xgo.mvvm.base.widget.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

/**
 * @author substring
 * @date 2019/6/3
 * email:xuan.zhang@xgo.one
 */
public abstract class BaseDialogFragment extends DialogFragment {

    /**
     * 自定义 dialog
     *
     * @return null 使用默认
     */
    @NonNull
    protected abstract View initView();

    @SuppressLint("ResourceType")
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(initView());
        return builder.create();
    }
}
