package com.xgo.mvvm.base.view;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.xgo.mvvm.base.lifecycle.BaseLifecycleObserver;

/**
 * @author substring
 * @date 2019/5/23
 * email:xuan.zhang@xgo.one
 */
public abstract class BaseActivity<VDataBind extends ViewDataBinding> extends AppCompatActivity {

    protected VDataBind mViewDataBinding;

    private BaseLifecycleObserver mLifecycleObserver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        mLifecycleObserver = initLifecycle();
        getLifecycle().addObserver(mLifecycleObserver);
        initViewModel();
        initView();
    }

    @Override
    protected void onDestroy() {
        if (mViewDataBinding != null) {
            mViewDataBinding.unbind();
        }
        if (mLifecycleObserver != null) {
            getLifecycle().removeObserver(mLifecycleObserver);
        }
        super.onDestroy();
    }

    /**
     * 初始化LayoutId
     */
    protected abstract int getLayoutId();

    /**
     * 初始化ViewModel
     */
    protected abstract void initViewModel();

    /**
     * 初始化视图
     */
    protected abstract void initView();

    /**
     * 初始化生命周期监听
     */
    protected abstract BaseLifecycleObserver initLifecycle();
}
