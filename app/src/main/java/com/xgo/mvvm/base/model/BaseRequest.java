package com.xgo.mvvm.base.model;

import java.io.Serializable;

/**
 * @author substring
 * @date 2019/5/27
 * email:xuan.zhang@xgo.one
 */
public class BaseRequest implements Serializable {

    public String key;
    public Object value;

    public BaseRequest(String _key, Object _value) {
        key = _key;
        value = _value;
    }
}
