package com.xgo.mvvm.base.widget.recyclerview;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author substring
 * @date 2019/6/3
 * email:xuan.zhang@xgo.one
 */
public abstract class BaseAdapter<T, VDataBind extends ViewDataBinding> extends RecyclerView.Adapter<BaseViewHolder> {

    private List<T> mListData;

    private int mVariableId;
    private int mLayoutId;

    public BaseAdapter(@LayoutRes int layoutId, int variableId) {
        mListData = new ArrayList<>();
        mVariableId = variableId;
        mLayoutId = layoutId;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        VDataBind dataBind = DataBindingUtil.inflate(inflater, mLayoutId, parent, false);
        return new BaseViewHolder(dataBind.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        VDataBind dataBind = DataBindingUtil.getBinding(holder.itemView);
        if (dataBind != null) {
            dataBind.setVariable(mVariableId, mListData.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    /**
     * 重置数据
     *
     * @param listData
     */
    public void refresh(List<T> listData) {
        mListData.clear();
        mListData.addAll(listData);
        notifyItemRangeChanged(0, listData.size());
    }

    /**
     * 添加数据
     *
     * @param data
     */
    public void add(T data) {
        mListData.add(data);
        notifyItemInserted(0);
    }

    /**
     * 删除数据
     *
     * @param position
     */
    public void remove(int position) {
        mListData.remove(position);
        notifyItemRemoved(position);
    }
}
