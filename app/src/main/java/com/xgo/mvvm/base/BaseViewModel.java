package com.xgo.mvvm.base;

import android.app.Application;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.xgo.mvvm.base.livedata.BaseLiveData;
import com.xgo.mvvm.base.model.BaseResponse;

/**
 * @author substring
 * @date 2019/5/22
 * email:xuan.zhang@xgo.one
 */
public abstract class BaseViewModel<BL extends BaseLiveData, VDataBind extends ViewDataBinding> extends AndroidViewModel {

    protected BL mLiveData;

    protected VDataBind mViewDataBinding;

    public BaseViewModel(@NonNull Application application) {
        super(application);
        mLiveData = initLiveData();
    }

    /**
     * 比较关键，用于与View通信
     */
    public abstract void injectionVDataBinding(VDataBind vDataBind);

    /**
     * 初始化LiveData
     */
    protected abstract BL initLiveData();
}
